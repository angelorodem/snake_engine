#include "ponto.h"


Ponto::Ponto()
{
}

Ponto::Ponto(int x, int y)
{
    this->x = x;
    this->y = y;
}

int Ponto::Getx(){
    return x;
}

int Ponto::Gety(){
    return y;
}

void Ponto::Setx(int x){
    this->x = x;
}

void Ponto::Sety(int y){
    this->y = y;
}

void Ponto::Setxy(int x,int y){
    this->x = x;
    this->y = y;
}
