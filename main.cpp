#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <glm.hpp>
#include "campo.h"
#include "ponto.h"

//#include <windows.h> Test later
//#include <GL/gl.h>



#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>

using namespace std;
using namespace glm;

static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}



GLFWwindow* iniciaGL(){    //---- Funtion to start GLFW
    GLFWwindow* window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(700, 700, "Snake", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, key_callback);

    return window;
}

int main()
{
    GLFWwindow* window = iniciaGL();



    Campo camp;           // ---- Campo is the snake field
    Campo::campo **cmp;
    cmp = camp.GetCampo();  //-- Get the pointer to the matrix (Field) that contains the information of each square in the field
    int ts=0,tst=0;
    while (!glfwWindowShouldClose(window))  // --- Game loop
    {
        float ratio;
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        //glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

       // glRotatef((float) glfwGetTime() * 50.f, 0.f, 0.f, 1.f);



//---------- TESTING

camp.apple();  //----- Puts an "apple" in a random position in the field
               //----- this makes random red squares appear in the screen

/*
ts++;
if(ts > 39){      //---- this will should fill the screen by inserting on red squeare at time
    ts=0;
    if(tst > 39){
        tst=0;
    }else{
    tst++;
    }
}

camp.mudaBloco(ts,tst,camp.APPLE);
*/
//----------------

//---- since opengl grid has the origin in the middle of the screen, i have to draw in each quadrant (++,+-,-+,--)
//---- thats why there is 4 fors below

for(int a=0;a<(camp.Getlar())/2;a++){//line of quadrant -+
            for(int b=0;b<(camp.Getalt())/2;b++){//column
                glBegin(GL_POLYGON);

                switch (cmp[a][b])
                {
                case camp.NADA:
                        glColor3f(0,0,0);
                    break;
                case camp.SNAKE:
                        glColor3f(0,0.85f,0.20f);
                    break;
                case camp.APPLE:
                        glColor3f(1.0f,0,0.1f);
                    break;
                case camp.MURO:
                        glColor3f(0.3f,0.3f,0.3f);
                    break;


                }


               glVertex2f(-1.0f+a/(camp.Getlar()/2.0f),        1.0f-b/(camp.Getalt()/2.0f) );
               glVertex2f(-1.0f+a/(camp.Getlar()/2.0f),        1.0f-(1+b)/(camp.Getalt()/2.0f) );
               glVertex2f(-1.0f+((1+a)/(camp.Getlar()/2.0f)),  1.0f-(1+b)/(camp.Getalt()/2.0f));
               glVertex2f(-1.0f+((1+a)/(camp.Getlar()/2.0f)),  1.0f-b/(camp.Getalt()/2.0f ));

               glEnd();
            }
        }



        for(int a=0;a<(camp.Getlar())/2;a++){//line of quadrant ++
            for(int b=0;b<(camp.Getalt())/2;b++){//column

                glBegin(GL_POLYGON);

                switch (cmp[a+camp.Getlar()/2][b])
                {
                case camp.NADA:
                        glColor3f(0,0,0);
                    break;
                case camp.SNAKE:
                        glColor3f(0,0.85f,0.20f);
                    break;
                case camp.APPLE:
                        glColor3f(1.0f,0,0.1f);
                    break;
                case camp.MURO:
                        glColor3f(0.3f,0.3f,0.3f);
                    break;


                }

               glVertex2f(a/(camp.Getlar()/2.0f),        1.0f-b/(camp.Getalt()/2.0f) );
               glVertex2f(a/(camp.Getlar()/2.0f),        1.0f-(1+b)/(camp.Getalt()/2.0f) );
               glVertex2f(((1+a)/(camp.Getlar()/2.0f)),  1.0f-(1+b)/(camp.Getalt()/2.0f));
               glVertex2f(((1+a)/(camp.Getlar()/2.0f)),  1.0f-b/(camp.Getalt()/2.0f) );

               glEnd();
            }
        }

        for(int a=0;a<(camp.Getlar())/2;a++){//line of quadrant +-
            for(int b=0;b<(camp.Getalt())/2;b++){//column

                glBegin(GL_POLYGON);

                switch (cmp[a+camp.Getlar()/2][b+camp.Getlar()/2])
                {
                case camp.NADA:
                        glColor3f(0,0,0);
                    break;
                case camp.SNAKE:
                        glColor3f(0,0.85f,0.20f);
                    break;
                case camp.APPLE:
                        glColor3f(1.0f,0,0.1f);
                    break;
                case camp.MURO:
                        glColor3f(0.3f,0.3f,0.3f);
                    break;


                }
               glVertex2f(a/(camp.Getlar()/2.0f),        -b/(camp.Getalt()/2.0f) );
               glVertex2f(a/(camp.Getlar()/2.0f),        -(1+b)/(camp.Getalt()/2.0f) );
               glVertex2f(((1+a)/(camp.Getlar()/2.0f)),  -(1+b)/(camp.Getalt()/2.0f));
               glVertex2f(((1+a)/(camp.Getlar()/2.0f)),  -b/(camp.Getalt()/2.0f ));

               glEnd();
            }
        }

        for(int a=0;a<(camp.Getlar())/2;a++){//line of quadrant --
            for(int b=0;b<(camp.Getalt())/2;b++){//column




                glBegin(GL_POLYGON);

                switch (cmp[a][b+camp.Getlar()/2])
                {
                case camp.NADA:
                        glColor3f(0,0,0);
                    break;
                case camp.SNAKE:
                        glColor3f(0,0.85f,0.20f);
                    break;
                case camp.APPLE:
                        glColor3f(1.0f,0,0.1f);
                    break;
                case camp.MURO:
                        glColor3f(0.3f,0.3f,0.3f);
                    break;


                }
               glVertex2f(-1.0f+a/(camp.Getlar()/2.0f),        -b/(camp.Getalt()/2.0f) );
               glVertex2f(-1.0f+a/(camp.Getlar()/2.0f),        -(1+b)/(camp.Getalt()/2.0f) );
               glVertex2f(-1.0f+((1+a)/(camp.Getlar()/2.0f)),  -(1+b)/(camp.Getalt()/2.0f));
               glVertex2f(-1.0f+((1+a)/(camp.Getlar()/2.0f)),  -b/(camp.Getalt()/2.0f ));

               glEnd();
            }
        }






        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);





    return 0;
}

