# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "C:/glfw/deps/glad.c" "C:/glfw/build_r/examples/CMakeFiles/heightmap.dir/__/deps/glad.c.obj"
  "C:/glfw/examples/heightmap.c" "C:/glfw/build_r/examples/CMakeFiles/heightmap.dir/heightmap.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLFW_DLL"
  "UNICODE"
  "WINVER=0x0501"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "../deps"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/glfw/build_r/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )
