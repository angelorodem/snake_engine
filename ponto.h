#ifndef PONTO_H
#define PONTO_H


class Ponto
{
private:
    int x,y;
public:
    Ponto();
    Ponto(int x,int y);
    int Getx();
    int Gety();
    void Setx(int x);
    void Sety(int y);
    void Setxy(int x,int y);
};

#endif // PONTO_H
