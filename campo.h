#ifndef CAMPO_H
#define CAMPO_H
#pragma once
#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>

class Campo
{
public:
    enum campo{NADA,SNAKE,APPLE,MURO}; // field may only contain these types {nothing, snake, Apple, wall}
    Campo(); //--- constructor
    ~Campo();//--- destructor
    campo **GetCampo(); //--- Field matrix getter
    void mudaBloco(int x, int y, campo val); //---- function that changes the block in a coordinates
    void apple(); //--- puts an apple at a "random" position
    void Setlar(int lar); //---- set max size x of the grid
    void Setalt(int alt); //---- set max size y of the grid
    int Getlar();
    int Getalt();

    struct ponto{  //---- this structure represents a dot in the field with the respective coordinates
        int x=0;
        int y=0;
    };

private:
    int apples=0;  //----- current number of apples
    int alt=40,lar=40; //---- default max x,y size
    campo **cmp; //--- Field matrix
    void vitoria(); //--- win function
    std::vector<ponto> campo_livre; //-- this vector maps the free spaces in the field, so instead of putting a random apple in the field, we put randomly only in the free spaces
};

#endif // CAMPO_H
