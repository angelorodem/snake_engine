TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    campo.cpp

QMAKE_CXXFLAGS += -std=c++14 -Ofast
INCLUDEPATH += C:/glfw/include
INCLUDEPATH += C:/glew/include
INCLUDEPATH += C:/glm/glm
LIBS +=-L"C:/glfw/build_r/src"
LIBS += -lglfw3 -lopengl32 -lglu32 -lgdi32
LIBS += -L"C:/glew/lib/Release/x64"
LIBS += -lglew32

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    campo.h

